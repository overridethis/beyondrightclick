using System;
using Xunit;

namespace Quakes.Core.Tests
{
    public class QuakesConfigurationTests
    {
        [Fact]
        public void Feed_settings_uses_right_key()
        {
            // Arrange, Act, Assert.
            AssertKeyIsValid(
                QuakesConfiguration.FeedKey,
                cfg => cfg.Feed);
        }


        [Fact]
        public void Detail_settings_uses_right_key()
        {
            // Arrange
            AssertKeyIsValid(
                QuakesConfiguration.DetailKey,
                cfg => cfg.Detail);
        }

        private static void AssertKeyIsValid(
            string keyNameValue,
            Func<IQuakesConfiguration, string> action)
        {
            var keyIsValid = false;
            var cfg = new QuakesConfiguration(kn =>
            {
                if (kn == keyNameValue)
                    keyIsValid = true;
                return "NOT_UNDER_TEST";
            });

            // Act.
            var value = action(cfg);

            // Assert.
            Assert.True(keyIsValid);
        }


    }
}
