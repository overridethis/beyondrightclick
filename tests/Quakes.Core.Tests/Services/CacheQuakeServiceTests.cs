﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using GeoJSON.Net.Feature;
using Moq;
using Quakes.Core.Persistence;
using Quakes.Core.Services;
using Xunit;

namespace Quakes.Core.Tests.Services
{
    public class CacheQuakeServiceTests
    {
        [Fact]
        public async Task Quake_gets_from_persistence_store_using_Id()
        {
            // Arrange.
            var collName = "COLLECTION_NAME";
            var docDb = new Mock<IDocDb>();
            var quakeId = "QUAKE_ID";
            var svc = new CacheQuakeService(docDb.Object, collName);

            // Act.
            var quake = await svc.QuakeAsync(quakeId);

            // Assert.
           docDb
                .Verify(s => s.ByIdAsync<Feature>(collName, quakeId), Times.Once);
        }

        [Fact]
        public async Task Quakes_gets_from_persistence_store()
        {
            // Arrange.
            var collName = "COLLECTION_NAME";
            var docDb = new Mock<IDocDb>();
            var svc = new CacheQuakeService(docDb.Object, collName);
            
            // Act.
            var quake = await svc.QuakesAsync();

            // Assert.
            docDb
                .Verify(s => s.QueryAsync<Feature>(collName), Times.Once);
        }
        
    }
}
