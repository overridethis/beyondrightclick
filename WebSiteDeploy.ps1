<#
 Requires Azure Authentication Prior to Execution.
 IMPORTANT: Customized for execution as an Azure Powershell Task on VSTS
#>
Param(
    [bool]$DoNuget = $true,
	[string]$WebsiteName,
	[string]$PackagePath
)

# USE CURRENT DIRECTORY OR VISUAL STUDIO TEAM SYSTEM "BUILD REPOSITORY LOCALPATH".
$basePath = if ($env:BUILD_REPOSITORY_LOCALPATH -eq $null) { '.' } else { $env:BUILD_REPOSITORY_LOCALPATH }

# RESTORE PUBLISH-MODULE 1.1.1
if ($DoNuget) {
    nuget install "$($basePath)\packages.config" -outputDirectory "$($basePath)\.deployment\packages"
}

# IMPORT MODULE
Import-Module "$($basePath)\.deployment\packages\publish-module.1.1.1\tools\publish-module.psm1"

# STOP AZURE WEBSITE
$azure = Get-AzureWebsite -Name $WebsiteName
Stop-AzureWebsite -Name $WebsiteName

Publish-AspNet -packOutput "$($PackagePath)" -publishProperties @{
	'WebPublishMethod'='MSDeploy';
	'MSDeployServiceURL'="$($WebsiteName).scm.azurewebsites.net:443";
	'DeleteExistingFiles'=$true;
	'SkipExtraFilesOnServer'=$false;
	'DeployIisAppPath'=$WebsiteName;
	'Username'=$azure.PublishingUsername;
	'Password'=$azure.PublishingPassword
}

# START AZURE WEBSITE
Start-AzureWebsite -Name $websiteName