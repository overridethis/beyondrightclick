Param(
    [string] [Parameter(Mandatory=$true)] $EnviromentResourceGroup
)

$appName = "$EnviromentResourceGroup-app-eastus2"
$apiName = "$EnviromentResourceGroup-api-eastus2"  
$resourceGroupName = "rg-$EnviromentResourceGroup-eastus2" 

# AppSettings (App)
$appSettings = @{ 
        "Usgs:Feed" = "http://$apiName.azurewebsites.net/api/quakes"; 
        "Usgs:Detail" = "http://$apiName.azurewebsites.net/api/quakes/detail/";
        "WEBSITE_NODE_DEFAULT_VERSION" = "6.9.1";
}
New-AzureRmResource -PropertyObject $appSettings -ResourceGroupName $resourceGroupName -ResourceType Microsoft.Web/sites/config -ResourceName "$EnviromentResourceGroup-app-eastus2/appsettings" -ApiVersion "2016-08-01" -Force

# AppSettings (Enable WebSockets)
$appProperties = Get-AzureRmResource -ResourceGroupName $resourceGroupName -ResourceType Microsoft.Web/sites/config -ResourceName "$EnviromentResourceGroup-app-eastus2/web" -ApiVersion "2016-08-01"
$appProperties.Properties.webSocketsEnabled = $true
Set-AzureRmResource -PropertyObject $appProperties -ResourceGroupName $resourceGroupName -ResourceType Microsoft.Web/sites/config -ResourceName "$EnviromentResourceGroup-app-eastus2/web" -ApiVersion "2016-08-01" -Force

# Get Cosmos Credentials
$docDbName = "cosmos-$EnviromentResourceGroup-eastus2"
$docDbKey = (Invoke-AzureRmResourceAction -Action listKeys -ResourceType "Microsoft.DocumentDb/databaseAccounts" -ApiVersion "2015-04-08" -ResourceGroupName $resourceGroupName -Name $docDbName -Force).primaryMasterKey
$docDbUri = "https://$docDbName.documents.azure.com:443/"
 
$apiSettings = @{ 
        "Usgs:Feed" = "http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/significant_week.geojson"; 
        "Usgs:Detail" = "https://earthquake.usgs.gov/earthquakes/eventpage/"; 
        "DocumentDb:Endpoint" = "$docDbUri"; 
        "DocumentDb:Key" = $docDbKey; 
        "DocumentDb:Database" = "quakes"; 
        "DocumentDb:Collection" = "significant";   
        "Notification:Quake" = "http://$appName.azurewebsites.net/api/hooks/quakes";
        "WEBSITE_NODE_DEFAULT_VERSION" = "6.9.1";
}
New-AzureRmResource -PropertyObject $apiSettings -ResourceGroupName $resourceGroupName -ResourceType Microsoft.Web/sites/config -ResourceName "$EnviromentResourceGroup-api-eastus2/appsettings" -ApiVersion "2016-08-01" -Force