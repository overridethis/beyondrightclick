﻿using GeoJSON.Net.Feature;
using Microsoft.Extensions.Configuration;
using Quakes.Core;
using Quakes.Core.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Quakes.Core.Persistence;

namespace Quakes.Jobs
{
    class Program
    {
        static IConfiguration Configuration;

        static void Main(string[] args)
        {
            Console.WriteLine("[Quakes:START] Synchronizing Quakes.");

            // configuration.
            Configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables()
                .Build();

            Task
                .Run(DoWork)
                .Wait();

            Console.WriteLine("[Quakes:END] Synchronizing Quakes.");
#if DEBUG
            Console.ReadKey();
#endif
        }

        private static async Task DoWork()
        {
            var cfg = new QuakesConfiguration(kn => kn == QuakesConfiguration.FeedKey
                ? Configuration.GetValue<string>("Usgs:Feed")
                : Configuration.GetValue<string>("Usgs:Detail"));
            var svc = new QuakeService(cfg);
            var quakes = await svc.QuakesAsync();

            var cache = new SimpleDocDb(
                Configuration.GetValue<string>("DocumentDb:Endpoint"),
                Configuration.GetValue<string>("DocumentDb:Key"),
                Configuration.GetValue<string>("DocumentDb:Database"));
            var collection = Configuration.GetValue<string>("DocumentDb:Collection");

            var cached = await cache.QueryAsync<Feature>(collection);
            var active = quakes.Features;

            // Add missing documents.
            var add = active
                .Where(a => cached.All(c => c.Id != a.Id))
                .ToList();

            Console.WriteLine($"[Quakes] Adding {add.Count()} Quakes.");

            foreach (var quake in add)
            {
                await cache.AddDocumentAsync(collection, quake);
            }
            await SendNotifications(add);

            // Remove other documents.
            var remove = cached
                .Where(a => active.All(c => c.Id != a.Id))
                .ToList();

            Console.WriteLine($"[Quakes] Removing {remove.Count()} Quakes.");

            foreach (var quake in remove)
                await cache.DeleteDocumentAsync(collection, quake.Id);
        }

        private static async Task SendNotifications(List<Feature> add)
        {
            try
            {
                var client = new SimpleHttp();
                var endpoint = new Uri(Configuration.GetValue<string>("Notification:Quake"));
                var quakeIds = add.Select(a => a.Id).ToArray();
                await client.PostAsync<string[]>(endpoint, quakeIds);
            }
            catch (Exception ex)
            {
                // Do Nothing.
            }
            
        }
    }
}