﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Quakes.Core.Services;

namespace Quakes.Browser.Controllers
{
    [Route("/api/[controller]")]
    public class QuakesController : Controller
    {
        private readonly IQuakeService service;

        public QuakesController(IQuakeService service)
        {
            this.service = service;
        }

        public async Task<IActionResult> Get()
        {
            return Json(await this.service.QuakesAsync());
        }

        [Route("[action]/{id}")]
        public async Task<IActionResult> Detail(string id)
        {
            return Json(await this.service.QuakeAsync(id));
        }

    }
}
