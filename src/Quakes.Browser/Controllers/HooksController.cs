﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Quakes.Core.Services;
using System.Collections.Generic;
using GeoJSON.Net.Feature;

namespace Quakes.Browser.Controllers
{
    [Route("/api/[controller]")]
    public class HooksController : Controller
    {
        private readonly IQuakeService service;

        public HooksController(IQuakeService service)
        {
            this.service = service;
        }

        [HttpPost]
        [Route("[action]")]
        public async Task<IActionResult> Quakes(
            [FromBody]string[] quakes)
        {
            var features = new List<Feature>();
            quakes = quakes ?? new string[] { };
            foreach(var quakeId in quakes)
            {
                var feature = await this.service.QuakeAsync(quakeId);
                features.Add(feature);
            }

            var msg = new { type = "Quake", quakes = features };
            await WebSocketManager.SendMessage(JsonConvert.SerializeObject(msg));
            return Ok();
        }

        [Route("[action]")]
        public async Task<IActionResult> Notification(
            string title,
            string message)
        {
            var msg = new { type = "Notification", message = message, title = title };
            await WebSocketManager.SendMessage(JsonConvert.SerializeObject(msg));
            return Ok();
        }

    }
}
