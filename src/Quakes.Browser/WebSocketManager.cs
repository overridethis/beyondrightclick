﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Quakes.Browser
{
    public static class WebSocketManager
    {
        static WebSocketManager()
        {
            Sockets = new List<WebSocket>();
        }

        private static List<WebSocket> Sockets { get; }

        public static void Add(
            WebSocket socket)
        {
            Sockets.Add(socket);
        }
        
        public static void Remove(
            WebSocket socket)
        {
            Sockets.Remove(socket);
        }

        public static async Task SendMessage(string msg)
        {
            foreach(var socket in Sockets)
            {
                //send each socket a message.
                if (socket.State != WebSocketState.Open)
                    continue;

                await socket.SendAsync(buffer: new ArraySegment<byte>(array: Encoding.ASCII.GetBytes(msg),
                                                                      offset: 0,
                                                                      count: msg.Length),
                                       messageType: WebSocketMessageType.Text,
                                       endOfMessage: true,
                                       cancellationToken: CancellationToken.None);
            }
        }
    }
}
