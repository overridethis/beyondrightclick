﻿angular
    .module("QuakesApp", ["uiGmapgoogle-maps"])
    .config(['uiGmapGoogleMapApiProvider', function (uiGmapGoogleMapApiProvider) {
        uiGmapGoogleMapApiProvider.configure({
            key: 'AIzaSyCaDVBhdfhkuihu9TfS3b323tP61JNT3YQ',
            v: '3.20', //defaults to latest 3.X anyhow
            libraries: 'weather,geometry,visualization'
        });
    }])
    .factory("Notifications", [function () {

        var onDataHandler = function () { };
        var onErrorHandler = function () { };
        var scheme = document.location.protocol == "https:" ? "wss" : "ws";
        var port = document.location.port ? (":" + document.location.port) : "";
        var uri = scheme + "://" + document.location.hostname + port;

        var socket;

        angular.element(document).ready(function () {
            console.log('websocket.init()')
            socket = new WebSocket(uri);
            socket.onopen = function (event) {
                console.log('websocket.open');
            };
            socket.onclose = function (event) {
                console.log('websocket.close');
            };
            socket.onerror = function (err) {
                console.log(err)
                onErrorHandler(err);
            };
            socket.onmessage = function (event) {
                console.log(event);
                onDataHandler(JSON.parse(event.data));
            };
        });

        return {
            onData: function (handler) {
                onDataHandler = handler;
            },
            onError: function (handler) {
                onErrorHandler = handler;
            }
        };

    }])
    .factory("QuakesService", ['$http', function ($http) {

        return {
            get: function () {
                return $http({
                    method: 'GET',
                    url: '/api/quakes',
                });
            },
            detail: function (id) {
                return $http({
                    method: 'GET',
                    url: '/api/quakes',
                });
            }
        };
    }])
    .controller("HomeCtrl", ['$scope', '$timeout', 'uiGmapGoogleMapApi', 'QuakesService', 'Notifications', function ($scope, $timeout, uiGmapGoogleMapApi, quakes, notifications) {

        var addQuake = function (quake, index) {

            var quake = {
                latitude: quake.geometry.coordinates[1],
                longitude: quake.geometry.coordinates[0],
                date: new Date(quake.properties.time),
                title: quake.properties.title,
                id: index,
                showWindow: false,
                options: {
                    animation: 1,
                    labelContent: 'Markers id ' + index,
                    labelAnchor: "22 0",
                    labelClass: "marker-labels"
                }
            };

            quake.closeClick = function () {
                quake.showWindow = false;
                $scope.$evalAsync();
            };
            quake.onClicked = function () {
                marker.showWindow = true;
                $scope.$apply();
            };

            $scope.quakes.push(quake);
        };

        var showMessage = function (title, message) {
            // Notifications.
            $scope.msg.message = message;
            $scope.msg.title = title;
            $scope.msg.show = true;
            $scope.$apply();
            $timeout(function () {
                $scope.msg.show = false;
                $scope.$apply();
            }, 10000);
        };

        $scope.msg = { show: false, message: '', title: '' };
        $scope.map = {
            center: { latitude: 47.4320, longitude: -88.1172 },
            zoom: 2,
            bounds: {
            }
        };
        $scope.quakes = [];
        $scope.error = undefined;

        notifications.onData(function (msg) {
            if (msg.type === 'Quake') {
                angular.forEach(msg.quakes, function (v, k) {
                    addQuake(v, $scope.quakes.length);
                });
                $scope.$apply();

                var title = 'New Earthquakes!';
                var message = '{0} new Earthquakes added to the Map';
                message = message.replace('{0}', msg.quakes.length);
                showMessage(title, message);

            } else {
                showMessage(msg.title, msg.message);
            }
        });

        notifications.onError(function (err) {
            $scope.error = err;
        });
        uiGmapGoogleMapApi.then(function (maps) {
            
            quakes
                .get()
                .then(function (rsp) {
                    angular.forEach(
                        rsp.data.features,
                        function (v, k) {
                            var quake = {
                                latitude: v.geometry.coordinates[1],
                                longitude: v.geometry.coordinates[0],
                                date: new Date( v.properties.time),
                                title: v.properties.title,
                                id: k,
                                showWindow: false,
                                options: {
                                    animation: 1,
                                    labelContent: 'Markers id ' + k,
                                    labelAnchor: "22 0",
                                    labelClass: "marker-labels"
                                }
                            };
                            
                            quake.closeClick = function () {
                                quake.showWindow = false;
                                $scope.$evalAsync();
                            };
                            quake.onClicked = function () {
                                marker.showWindow = true;
                                $scope.$apply();
                            };

                            $scope.quakes.push(quake);

                        });
                }, function (err) {
                    $scope.err = "ERROR: :( Something went wrong.";
                });
        });
    }]);