﻿using System;

namespace Quakes.Core
{
    public class QuakesConfiguration : IQuakesConfiguration
    {
        public static readonly string FeedKey = "Feed";
        public static readonly string DetailKey = "Detail";

        private readonly Func<string, string> _config;

        public QuakesConfiguration(
            Func<string, string> config)
        {
            _config = config;
        }

        public string Feed => _config(FeedKey);
        public string Detail => _config(DetailKey);
    }
}