﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;
using System.Net.Http.Headers;
using System.Text;

namespace Quakes.Core
{
    public class SimpleHttp : IHttp
    {
        private static readonly IDictionary<string, string> EmptyDictionary = new Dictionary<string, string>();

        public async Task<TResponseType> GetAsync<TResponseType>(
            Uri uri,
            Dictionary<string, string> headers = null)
        {
            using (var client = new HttpClient())
            {
                var req = new HttpRequestMessage()
                {
                    Method = HttpMethod.Get,
                    RequestUri = uri
                };

                foreach (var header in headers ?? EmptyDictionary)
                    req.Headers.Add(header.Key, header.Value);

                var rsp = await client.SendAsync(req);


                // TODO: Error Handling.
                var content = await rsp.Content.ReadAsStringAsync();

                return JsonConvert.DeserializeObject<TResponseType>(content);
            }
        }

        public async Task<HttpStatusCode> PostAsync<TRequestBodyType>(
            Uri uri,
            TRequestBodyType body,
            Dictionary<string, string> headers = null)
        {
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders
                    .Accept
                    .Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var req = new HttpRequestMessage()
                {
                    Method = HttpMethod.Post,
                    RequestUri = uri,
                    Content = new StringContent(JsonConvert.SerializeObject(body),
                        Encoding.UTF8,
                        "application/json")
                };

                
                
                foreach (var header in headers ?? EmptyDictionary)
                    req.Headers.Add(header.Key, header.Value);

                var rsp = await client.SendAsync(req);

                return rsp.StatusCode;
            }
        }
    }
}
