﻿namespace Quakes.Core
{
    public interface IQuakesConfiguration
    {
        string Feed { get; }

        string Detail { get; }
    }
}