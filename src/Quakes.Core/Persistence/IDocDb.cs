﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Quakes.Core.Persistence
{
    public interface IDocDb
    {
        Task AddDocumentAsync(
            string collectionName,
            object document);

        Task DeleteDocumentAsync(
            string collectionName,
            string documentId);

        Task<IEnumerable<TDocument>> QueryAsync<TDocument>(
            string collectionName);

        Task<TDocument> ByIdAsync<TDocument>(
            string collectionName,
            string documentId);
    }
}