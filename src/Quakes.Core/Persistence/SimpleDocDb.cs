﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;

namespace Quakes.Core.Persistence
{
    public class SimpleDocDb : IDocDb
    {
        private readonly Lazy<DocumentClient> _client;
        private readonly Lazy<Task<Database>> _database;
        private readonly string _databaseName;

        public SimpleDocDb(
            string accountEndpoint,
            string accountKey,
            string databaseName)
        {
            _databaseName = databaseName;
            _client = new Lazy<DocumentClient>(() => new DocumentClient(new Uri(accountEndpoint), accountKey));

            _database = new Lazy<Task<Database>>(async () =>
            {
                var database = new Database { Id = databaseName };
                var response = await _client.Value.CreateDatabaseIfNotExistsAsync(database);
                return response.Resource;
            });
        }

        public async Task AddDocumentAsync(
            string collectionName,
            object document)
        {
            await EnsureCollectionExistsAsync(collectionName);

            await _client.Value.CreateDocumentAsync(
                UriFactory.CreateDocumentCollectionUri(_databaseName, collectionName), document);
        }

        private async Task EnsureCollectionExistsAsync(string collectionName)
        {
            // Ensure Database Exists.
            var database = await _database.Value;

            // Ensure Collection Exists.
            await _client.Value.CreateDocumentCollectionIfNotExistsAsync(
                database.SelfLink,
                new DocumentCollection
                {
                    Id = collectionName
                });
        }

        public async Task DeleteDocumentAsync(
            string collectionName,
            string documentId)
        {
            await EnsureCollectionExistsAsync(collectionName);

            var documentUri = UriFactory.CreateDocumentUri(
                _databaseName,
                collectionName,
                documentId);

            await _client.Value.DeleteDocumentAsync(documentUri);
        }

        public async Task<IEnumerable<TDocument>> QueryAsync<TDocument>(
            string collectionName)
        {
            await EnsureCollectionExistsAsync(collectionName);

            var documentUri = UriFactory.CreateDocumentCollectionUri(_databaseName, collectionName);

            return _client.Value
                .CreateDocumentQuery<TDocument>(documentUri)
                .ToList();
        }

        public async Task<TDocument> ByIdAsync<TDocument>(
            string collectionName,
            string documentId)
        {
            await EnsureCollectionExistsAsync(collectionName);

            var documentUri = UriFactory.CreateDocumentCollectionUri(
                _databaseName,
                collectionName);

            var expr = $"SELECT * FROM r WHERE r.id = '{documentId}'";
            var doc = _client.Value.CreateDocumentQuery<TDocument>(documentUri, expr);

            return doc
                .ToList()
                .FirstOrDefault();
        }

        public async Task ClearCollectionAsync(string collectionName)
        {
            await EnsureCollectionExistsAsync(collectionName);
            await ClearCollectionOneByOneAsync(collectionName);
        }
        
        private async Task ClearCollectionOneByOneAsync(string collectionName)
        {
            var database = await _database.Value;
            var collection = _client.Value.CreateDocumentCollectionQuery(database.CollectionsLink)
                .ToList()
                .First();
            var docs = _client.Value.CreateDocumentQuery(collection.DocumentsLink);
            foreach (var doc in docs)
                await _client.Value.DeleteDocumentAsync(doc.SelfLink);
        }
    }
}
