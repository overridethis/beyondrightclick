﻿using GeoJSON.Net.Feature;
using System.Threading.Tasks;

namespace Quakes.Core.Services
{
    public interface IQuakeService
    {
        Task<FeatureCollection> QuakesAsync();

        Task<Feature> QuakeAsync(string quakeId);
    }
}