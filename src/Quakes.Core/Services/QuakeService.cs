﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using GeoJSON.Net.Feature;

namespace Quakes.Core.Services
{
    public class QuakeService : IQuakeService
    {
        private IQuakesConfiguration _config;

        public QuakeService(
            IQuakesConfiguration config)
        {
            this._config = config;
        }

        public async Task<FeatureCollection> QuakesAsync()
        {
            var uri = new Uri(_config.Feed);
            return await new SimpleHttp().GetAsync<FeatureCollection>(uri);
        }

        public async Task<Feature> QuakeAsync(string quakeId)
        {
            var uri = new Uri($"{_config.Detail}{quakeId}");
            return await new SimpleHttp().GetAsync<Feature>(uri);
        }
    }

}
