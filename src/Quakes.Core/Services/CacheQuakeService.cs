﻿using System;
using System.Threading.Tasks;
using GeoJSON.Net.Feature;
using System.Linq;
using Quakes.Core.Persistence;

namespace Quakes.Core.Services
{
    public class CacheQuakeService : IQuakeService
    {
        private readonly IDocDb _docDb;
        private readonly string _collectionName;

        public CacheQuakeService(
            IDocDb docDb,
            string collectionName)
        {
            this._docDb = docDb;
            this._collectionName = collectionName;
        }

        public Task<Feature> QuakeAsync(string quakeId)
        {
            var quake = _docDb.ByIdAsync<Feature>(this._collectionName, quakeId);

            return Task.Run(() => quake);
        }

        public async Task<FeatureCollection> QuakesAsync()
        {
            var features = await _docDb
                .QueryAsync<Feature>(this._collectionName);

            return new FeatureCollection(features.ToList());
        }
    }
}
