﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;

namespace Quakes.Core
{
    public interface IHttp
    {
        Task<TResponseType> GetAsync<TResponseType>(
            Uri uri,
            Dictionary<string, string> headers = null);

        Task<HttpStatusCode> PostAsync<TRequestBodyType>(
            Uri uri,
            TRequestBodyType body,
            Dictionary<string, string> headers = null);
    }
}