$basePath = if ($env:BUILD_REPOSITORY_LOCALPATH -eq $null) { "$pwd" } else { $env:BUILD_REPOSITORY_LOCALPATH }
$artifactsPath = "$basePath\.deployment\artifacts\"

Remove-Item -Path "$basePath\.deployment" -Force -Recurse -ErrorAction Ignore

dotnet publish "$basePath\src\Quakes.Browser\Quakes.Browser.csproj" --configuration Release --output "$artifactsPath\browser\"
dotnet publish "$basePath\src\Quakes.Proxy\Quakes.Proxy.csproj" --configuration Release --output "$artifactsPath\proxy\"
dotnet publish "$basePath\src\Quakes.Jobs\Quakes.Jobs.csproj" --configuration Release --output "$artifactsPath\proxy\App_Data\jobs\triggered\Quakes.Jobs\"