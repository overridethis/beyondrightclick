<#
    Full Cycle Deployment.
#>
Param([string]$resourcePrefix = "demo")

# PACKAGE APP
.\PackageApp.ps1

$appName = "$resourcePrefix-app-eastus2"
$apiName = "$resourcePrefix-api-eastus2"
$appPackage = ".\.deployment\artifacts\browser"
$apiPackage = ".\.deployment\artifacts\proxy"

# DEPLOY APP
.\WebSiteDeploy.ps1 -WebsiteName $appName -PackagePath $appPackage

# DEPLOY API
.\WebSiteDeploy.ps1 -WebsiteName $apiName -PackagePath $apiPackage

# CONFIG ENVIRONMENT
.\WebSiteConfig.ps1 -EnviromentResourceGroup $resourcePrefix